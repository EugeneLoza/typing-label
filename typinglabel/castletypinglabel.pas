unit CastleTypingLabel;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleControls, CastleClassUtils;

type
  TCastleTypingLabel = class(TCastleLabel)
  strict private const
    DefaultTypingSpeed = 60;
  strict private
    FTimer: Single;
    FTypingSpeed: Single;
  public
    procedure ResetText;
    procedure ShowAllText;
    function FinishedTyping: Boolean;
    function PropertySections(const PropertyName: String): TPropertySections; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  published
    property TypingSpeed: Single read FTypingSpeed write FTypingSpeed default DefaultTypingSpeed; //chars per second
  end;

implementation
uses
  CastleComponentSerialize;

function TCastleTypingLabel.PropertySections(
  const PropertyName: String): TPropertySections;
begin
  case PropertyName of
    'TypingSpeed':
      Result := [psBasic]
    else
      Result := inherited PropertySections(PropertyName);
  end;
end;

procedure TCastleTypingLabel.Update(const SecondsPassed: Single; var HandleInput: Boolean);
var
  N: Integer;
begin
  inherited;
  FTimer += SecondsPassed;
  N := Round(FTimer * TypingSpeed);
  if N < DisplayChars then
    MaxDisplayChars := N
  else
    MaxDisplayChars := -1; //show all
end;

procedure TCastleTypingLabel.ResetText;
begin
  FTimer := 0;
end;

procedure TCastleTypingLabel.ShowAllText;
begin
  FTimer := 999;
end;

function TCastleTypingLabel.FinishedTyping: Boolean;
begin
  Result := Round(FTimer * TypingSpeed) >= DisplayChars;
end;

constructor TCastleTypingLabel.Create(AOwner: TComponent);
begin
  inherited;
  FTypingSpeed := DefaultTypingSpeed;
  ResetText;
end;

initialization
  RegisterSerializableComponent(TCastleTypingLabel, 'Typing Label');
end.

