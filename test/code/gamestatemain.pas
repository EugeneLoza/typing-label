unit GameStateMain;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse,
  CastleTypingLabel;

type
  { Main state, where most of the application logic takes place. }
  TStateMain = class(TUIState)
  private
    TypingLabel1: TCastleTypingLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  StateMain: TStateMain;

implementation

uses SysUtils;

{ TStateMain ----------------------------------------------------------------- }

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gamestatemain.castle-user-interface';
end;

procedure TStateMain.Start;
begin
  inherited;

  { Find components, by name, that we need to access from code }
  TypingLabel1 := DesignedComponent('TypingLabel1') as TCastleTypingLabel;
end;

function TStateMain.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys

  if Event.EventType = itMouseButton then
  begin
    if TypingLabel1.FinishedTyping then
      TypingLabel1.ResetText
    else
      TypingLabel1.ShowAllText;
  end;
end;

end.
